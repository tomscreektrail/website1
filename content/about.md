+++
title = "About"
date = "2014-04-09"
aliases = ["about-us","about-hugo","contact"]
[ author ]
  name = "Hugo Authors"
+++

Trails are **adventurous**, help development of trails in Blacksburg for the community.

This website is created to take feedback from the Blacksburg residents for trail connectivity project:

Our Team:

* Dalton Nelson
* David Bruce
* Karen Williams
* Matthew Lindsay
* William Paul Chilton
* Kuldeep Dixit

Learn more about the project by contacting Prof. [*Todd Schenk*](https://spia.vt.edu/people/faculty/schenk.html).
